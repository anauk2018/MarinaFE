function printAndGetHighScore(scores) {
    let highScore = 0;
    let output;
    for (let i = 0; i < scores.length; i++) {
        output = "Bubble solution# " + i + " score: " + scores[i];
        console.log(output);
        if (scores[i] > highScore) {
            highScore = scores[i];
        }
    }
    return highScore;
}

function getBestResults(scores, highScore) {
    let bestSolutions = [];
    for (let i = 0; i < scores.length; i++) {
        if (scores[i] == highScore) {
            bestSolutions.push(i);
        }
    }
    return bestSolutions;
}
function getMoreCostEffectiveSolutuin (scores, costs, highScore) {
    let cost = 100;
    let index;
    for (let i=0; i<scores.length; i++) {
        if (scores[i] == highScore && cost > costs[i]) {
                index = i;
                cost = costs[i];
            }        
    }
    return index;
}
//массив-список растворов;
let scores = [45, 45, 50, 90,
             35, 67, 89, 90,
             15, 34, 24, 13];

let highScore = printAndGetHighScore(scores);
console.log("Bubble test:" + scores.length);
console.log("Highest buuble score: " + highScore);

let bestSolutions = getBestResults(scores, highScore);
console.log("Solutions with the highest score: " + bestSolutions);

//массив-список затрат на изготовление растворов;
let costs = [.24, .25, .24, .26,
            .21, .23, .24, .25,
            .26, .23, .23, .26];

let mostCostEffective = getMoreCostEffectiveSolutuin(scores, costs, highScore);
console.log("Bubble solution #" + mostCostEffective + " is most cost effective");