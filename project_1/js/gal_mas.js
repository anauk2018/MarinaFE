$(document).ready(function () {
    jQuery('.item-massonry').hover(
        function () {
            $(this).find('.cover-item-gallery').fadeIn();
        },
        function () {
            $(this).find('.cover-item-gallery').fadeOut();
        }
    );
    
    let sizer = '.sizer4';
    
    let container = $('#gallery');
    container.imagesLoaded(function () {
        container.masonry({
            itemSelector: '.item-massonry',
            columnWidth: sizer,
            /*percentPosition: true,*/
            isFitWidth: true
        });
    });
});
