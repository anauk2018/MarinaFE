function genertorCar() {
    let models = ["Shevrole", "Ferary", "Chery"];
    let colors = ["red", "blue", "green"];
    let years = [1999, 2000, 2001];
    let moldis = [true, false];

    let rand1 = Math.floor(Math.random() * models.length);
    let rand2 = Math.floor(Math.random() * colors.length);
    let rand3 = Math.floor(Math.random() * years.length);
    let rand4 = Math.floor(Math.random() * 2);
    let rand5 = Math.floor(Math.random() * 3) + 1;

    let car = {
        model: models[rand1],
        color: colors[rand2],
        year: years[rand3],
        moldi: models[rand4],
        pasenger: [rand5],
        convertible: 0,
        drive: function () {
            alert("Zoom zoom!");
        }
    };
    return car;
}

function displayCar(car) {
    console.log("Your new car is a " + car.year + " " + car.model + " " + car.color + " " + car.drive);
}
let carToSell = genertorCar();
displayCar(carToSell);

let fiat = {
        model: "Gard",
        color: "green",
        year: 1994,
        moldi: true,
        pasenger: 5,
        convertible: true,
        started: false,
    
        start: function () {
                    this.started = true;
                },
    
        stop: function () {
                    this.started = false;
                },
    
        drive: function () {
                    if (this.started) {
                        alert("Zoomzoomzooom !");
                    } else {
                        alert("No zoom!");
                    }
                }
        };
    fiat.drive();
