$(function () {
    function slowScroll (e) {
        let $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - -10
        }, 900);
        e.preventDefault();
    }
    $('a.menu-link').on("click", slowScroll);
    
    $('.btn-slide'). click(function() {
        $('#panel').slideToggle('slow');
        $(this).toggleClass('active');
        return false;
    });
    
    $(window).scroll(function() {
        if($(this).scrollTop() != 0){
            $('#toTop').fadeIn();            
        } else {
            $('#toTop').fadeOut();
        }
    });
    
    $('#toTop').click(function() {
        $('body, html').animate({scrollTop:0},800);
    });
   
});