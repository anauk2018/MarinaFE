/*Считать с помощью модального окна браузера число, которое введет пользователь. 
Вывести в консоли все простые числа от 1 до введенного пользователем числа.
Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.

Не обязательное задание продвинутой сложности:

Проверить, что введенное значение является целым числом, и больше единицы. Если данные условия не соблюдаются, повторять вывод окна на экран до тех пор, пока не будет введено целое число больше 1.
Максимально оптимизировать алгоритм, чтобы он выполнялся как можно быстрее для больших чисел.
Считать два числа, m и n. Вывести в консоль все простые числа в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n). Если хотя бы одно из чисел не соблюдает условия валидации, указанные выше, вывести сообщение об ошибке, и спросить оба числа заново. */
/*function easyNumber(num) {
    let number = '';
    for (let i = 1; i < num; i++) {
        isSimple = true;
        for (let l = 2; l < i; l++) {
            if (i % l == 0) {
                isSimple = false;
                break;
            }
        }
        if (isSimple) {
            number += String(i) + " ";
        }
    }
    console.log(number);
    return true;
}


let number = +prompt('Enter number!', '');
easyNumber(number);*/
/*
function easyNumber(num) {
    if (num < 2) return false;
    for (let i = 2; i < num; i++) {
        if (num % i == 0) return false;
    }
    return true;
}
function diapazon(m, n){
let result = [];
if(m > n){
    let tmp = m;
    m = n;
    n = tmp;
}
for (let i = m; i < n; i++) {
    if (easyNumber(i)) {
        result.push(i);        
    }    
}
return result;
}
let numberM = +prompt('Enter start number!', '');
let numberN = +prompt('Enter end number!', '');

console.log(diapazon(numberM, numberN));
*/

function easyNumber(num) {
    if (num < 2) return false;
    for (let i = 2; i < num; i++) {
        if (num % i == 0) return false;
    }
    return true;
}
function printDiapazon(m, n){
let result = [];
for (let i = m; i < n; i++) {
    if (easyNumber(i)) {
        result.push(i);        
    }    
}
return result;
}
function getUserInput(invitation) {
    let userInput = prompt(invitation);
    if(userInput != null) {
        if(userInput = validateInput(userInput)){
            console.log(userInput);
            return userInput;
        } else {
            console.error("Wrong number format! Try again!");
            getUserInput(invitation);
        }
    }
    return false;
}
function validateInput(userInput) {
    if((userInput = Number(userInput)) && Number.isInteger(userInput) && userInput >= 1) {
        console.log(typeof(userInput));
       return userInput;
    } 
    return false;    
}

let minNumber = getUserInput('Enter min number!','');
let maxNumber = getUserInput('Enter max number!','');

while (maxNumber<=minNumber) {
    maxNumber= prompt('Введите число большее '+ minNumber);
}
console.log(printDiapazon(minNumber, maxNumber));