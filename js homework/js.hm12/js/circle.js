/*Нарисовать на странице круг используя параметры, которые введет пользователь.

Технические требования:

При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript.
При нажатии на кнопку - вместо нее показать два поля ввода. В первом пользователь сможет ввести диаметр круга в пикселях. Во втором - цвет круга (в любом формате, который понимает CSS - имя цвета, RGB, HEX, HSL).
Под полями ввода должна быть кнопка "Нарисовать". При нажатии - на странице должен появиться круг заданного пользователем диаметра и с заливкой указанного цвета.


Не обязательное задание продвинутой сложности:

При нажатии на кнопку "Нарисовать круг" показывать только одно поле ввода - диаметр круга. При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на каждый - они должны исчезать
У вас может возникнуть желание поставить обработчик события на каждый круг для его исчезновения. Это неэффективно, так делать не нужно. На всю страницу должен быть только один обработчик событий, который будет это делать.*/
/*
let drow = document.getElementsByClassName('drow');
drow = +prompt('Enter radius','');
let circle = document.getElementsByClassName('circle')[0];
circle.style = `background: yellow;
border-radius: 50%;
width: 200px;
height: 200px;`;*/
// Показать полупрозрачный DIV, затеняющий всю страницу
    // (а форма будет не в нем, а рядом с ним, чтобы не полупрозрачная)
    /*function showCover() {
      let coverDiv = document.createElement('div');
      coverDiv.id = 'cover-div';
      document.body.appendChild(coverDiv);
        console.log(coverDiv);
    }*/

    function hideCover() {      document.body.removeChild(document.getElementById('cover-div'));
    }

    function showPrompt(text, callback) {
      showCover();
      let form = document.getElementById('prompt-form');
      let container = document.getElementById('prompt-form-container');
      document.getElementById('prompt-message').innerHTML = text;
      form.elements.text.value = '';

      function complete(value) {
        hideCover();
        container.style.display = 'none';
        document.onkeydown = null;
        callback(value);
      }

      form.onsubmit = function() {
        let value = form.elements.text.value;
        if (value == '') return false; // игнорировать пустой submit

        complete(value);
        return false;
      };

      form.elements.cancel.onclick = function() {
        complete(null);
      };

      document.onkeydown = function(e) {
        if (e.keyCode == 27) { // escape
          complete(null);
        }
      };

      let lastElem = form.elements[form.elements.length - 1];
      let firstElem = form.elements[0];

      lastElem.onkeydown = function(e) {
        if (e.keyCode == 9 && !e.shiftKey) {
          firstElem.focus();
          return false;
        }
      };

      firstElem.onkeydown = function(e) {
        if (e.keyCode == 9 && e.shiftKey) {
          lastElem.focus();
          return false;
        }
      };

      container.style.display = 'block';
//      form.elements.text.focus();
    }

    document.getElementById('show-button').onclick = function() {
      showPrompt("Введите радиус круга:)", function(value) {
        drowCircle("Вы ввели: " + value);
      });
    };


document.getElementById('drow').onclick = function() {
    
    let circleRadius = document.getElementById("circle-radius").value;
    let circleColor = document.getElementById("circle-color").value;
  console.log(circleColor);
    let circleStyle = `background: ${circleColor};
border-radius: 50%;
width: ${circleRadius}px;
height: ${circleRadius}px;
display: inline-block;s`;
   
    let circleContainer = document.getElementById("circle-container");
    for(let i= 0; i <100; i++) {
        let circle = document.createElement('div');
        circle.style = circleStyle;        
        circleContainer.appendChild(circle);
        console.log(circle);
    }
}