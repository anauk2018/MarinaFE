/*Реализовать функцию нахождения уникальных объектов в массиве при сравнении с другим массивом, по указанному пользователем свойству.

Технические требования:


Написать функцию excludeBy(), которая в качестве первых двух параметров будет принимать на вход два массива с объектами любого типа, например:
const users = [{
  name: "Ivan",
  surname: "Ivanov",
  gender: "male",
  age: 30
},
{
    name: "Anna",
    surname: "Ivanova",
    gender: "female",
    age: 22
}]


В качестве третьего параметра функция должна принимать имя поля, по которому будет проводиться сравнение.


Функция должна вернуть новый массив, который будет включать те объекты из первого массива, значение указанного свойства которых не встречается среди объектов, представленных во втором массиве. Например, вызов функции excludeBy(peopleList, excluded, 'name') должен возвращать массив из тех пользователей peopleList, имя которых (свойство name) не встречается у пользователей, которые находятся в массиве excluded.*/

/*function excludeBy(obj1, obj2, objName) {
    
    let excluded = " ";
    excluded.map(function(objName) {
  return excluded;
});
    for (var i = 0; i < users.length; i++) {    
      excluded += i = obj[i] + "\n";    
  }
    for (var i = 0; i < peoples.length; i++) {    
       excluded += i = obj[i] + "\n";    
  }
   
}

const users = [{
  name: "Ivan",
  surname: "Ivanov",
  gender: "male",
  age: 30
},
{
    name: "Anna",
    surname: "Ivanova",
    gender: "female",
    age: 22
}];
const peoples = [{
  name: "Shasha",
  surname: "Ivanov",
  gender: "male",
  age: 31
},
{
    name: "Anna",
    surname: "Ivanova",
    gender: "female",
    age: 22
}];
let result = excludeBy(users, peoples, name);
console.log(result);*/
//способы создания обектов
//1-через литерал
let obj1 = {
    name: 'vasa',
    surname: 'danilisuin',
    age: 25,
    'own information': {
        wife: 'Kata',
        cildren: 2,
        perents: {
            fatherName:'Roma',
            motherName: 'Nina',
            sisters: {
                name1: 'Ola',
                name2: "alla"
            }
        },
        hz: [1, 2, 3]
    }
};
console.log(obj1);
obj1.email = 'mail.ru';
//2.
let obj = new Object();
console.log(obj);
console.log(obj === obj1);
//2. метод create,с указвнием обекта-прототипа
let obj3 = Object.create(obj1);
console.log(obj3);

function addObj(o, i, v) {
    return o[i] = v;
}
addObj(obj3, 'car', 'BMV');
addObj(obj3, 'prophesia', 'hendler');
console.log(obj3);
console.log(obj3.name);
//цикл для прохода по свойствам обекта 1 вложености
/*for (let v in obj1) {
    console.log(v + '-' + obj1[v]);
}*/

//цикл для прохода по свойствам обекта 2 вложености
function interation(o) {
    for(v in o) {
        if(typeof o[v] == 'object') {
            interation(o[v]);
        } else {
        console.log(v+':'+o[v]);
        }
    }
}
interation(obj1);

//проверка наличия свойств у обекта
//1.
if (obj1.name) {
    console.log(obj1.name);
}
//2.
if('age' in obj1) {
    console.log(obj1.age);
}
//3.
if(obj1.hasOwnProperty('surname')) {
    console.log(obj1.surname);
}
//3.a для наследуемых своейств hasOwnProperty - не подходит
if(obj3.hasOwnProperty('own information')) {
    console.log(obj3['own information']);
} else {
    console.log(false);
}
//4.
if(obj3.propertyIsEnumerable('own information')) {
   console.log(obj3['own information']);
   }
//5.
if(obj1.name !== undefined) {
    console.log(obj1.name);
}
// присвоение функций обекту:
//1.
let qwerty = {
    func: function(){
        console.log('hay!')
    }
}
qwerty.func();
//2.
qwerty.func2 = function(){
    console.log('how are you!');
}
qwerty.func2();
//3.
function foo() {
    console.log('i do it!');
}
qwerty.func3 = foo;
qwerty.func3();

https://coursehunters.net/course/kurs-po-javascript-osnovy
// Наша функция сравнения
/*unction excludeBy(users, peoples) {
  console.log(users.age - peoples.age);
}

// проверка


let people = [users , peoples];

people.sort(excludeBy);

// вывести
for(let i = 0; i < people.length; i++) {
  console.log(people[i].name); 
}*/
/*function propertyObj (obj, objName) {
    let result =" ";
    for(let i in obj) {
        if(obj.hasOwnProperty(i)) {
             result += objName + "." + i + "="+ obj[i] + "\n";
        }
    }
    return result;
}
var myCar = new Object();
myCar.make = "Ford";
myCar.model = "Mustang";
myCar.year = 1969;
console.log(propertyObj(myCar,"myCar"));*/
