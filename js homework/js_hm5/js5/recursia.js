/*Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
Использовать синтаксис ES6 для работы с перемеными, функциями и объектами.*/

function cloneObject(obj) {
    let clone = {};
    for (let i in obj) {
        if (typeof (obj[i]) == "object" && obj[i] != null) {
            clone[i] = cloneObject(obj[i]);
        } else {
            clone[i] = obj[i];
        }
    }
    return clone;
}

/*function deepCopy(obj) {
    if(Object.prototype.toString.call(obj) === '[object Array]') {
        let out = [],i=0, len = obj.length;
        for(;i<len;i++){
            out[i] = arguments.callee(obj[i]);
        }
        return out;
    }
    if(typeof obj === 'object') {
        let out = {}, i;
        for (i in obj) {
            out[i] = arguments.callee(obj[i]);
        }
        return out;
    }
    return obj;
}*/

let obj = {
    g: 2,
    h: [[[5]], ['gg']],
    c: {
        f: 5,
        g: [{
            g: {
                g: 5,
                h: 7,
                g: [1, {
                    g: {
                        jl: 5,
                        kl: null,
                        dd: new Date(),
                        lk: undefined,
                        ff: [],
                        rm: /.+/
                    }
                }]
            }
        }]
    }
};

console.log(obj);
console.log(obj.c.g[0].g);
console.log(cloneObject(obj));
//let copy = deepCopy(obj);
//copy.g = 10;
/*copy.c = {
    a: 78,
    b: 56
};*/
//console.log(copy);