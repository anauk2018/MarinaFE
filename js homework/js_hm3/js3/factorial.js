/*Написать функцию подсчета факториала числа.
Считать с помощью модального окна браузера число, которое введет пользователь.
С помощью функции посчитать факториал числа, которое ввел пользователь, и вывести его на экран.
Использовать синтаксис ES6 для работы с перемеными и функциями.*/

function factorial(n) {
    /*1- способ: let result = 1;
for (let i=1; i<=n;i++){
    result = result*i;
}
    console.log(result);
    return result;
   2-й способ: return (n !=1)?n*factorial(n-1):1;*/
    return n ? n * factorial(n - 1) : 1;
}
let number = +prompt('Please enter number', '');
document.write(factorial(number));
