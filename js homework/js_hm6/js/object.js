/*Реализовать функцию-конструктор для создания объекта "пользователь".
Технические требования:
Написать функцию createNewUser(), которая будет создавать и возвращать объект "пользователь".
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект со свойствами firstName и lastName.
Добавить в объект метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре.
Не обязательное задание продвинутой сложности:
Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свйоства.*/
function User(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.getLogin = function () {
        return this.firstName.split(' ').map(firstName => firstName[0].toUpperCase()).join('') + this.lastName.toLowerCase();
    }    

}
let user = new User(prompt("Enter your firstname", ""), prompt("Enter your lastname", ""));
alert(user.getLogin());
/*let user = {
    //one: null,
    //two: null,    
    createNewUser: function () {   
        this.firstname = (prompt("Enter firstname"));
        this.lastname = (prompt("Enter lastname"));
    },
    getLogin: function () {
        return this.firstName.split(' ').map(firstName => firstName[0].toUpperCase()).join('') + this.lastname.toLowerCase();
    }    
}
user.createNewUser();
console.log(user);
console.log("Object user" + user.createNewUser());
alert("Invitation" + user.getLogin());*/

/*function createNewUser(){
    
}
let user = new Object();
user.firstname = prompt("Enter firstname");
user.lastname = prompt("Enter lastname");
user.getLogin = function() {
    return this.firstname.split(' ').map(firstName => firstName[0].toUpperCase()).join('') + this.lastname.toLowerCase();
}
console.log(user.firstname);
console.log(user.lastname);
console.log(user);
console.log(user.getLogin());*/

/*function createNewUser() {
    return {
        firstname: prompt("Enter firstname"),
        lastname: prompt("Enter lastname"),
        getLogin: function () {
            return this.firstname.split(' ').map(firstName => firstName[0].toUpperCase()).join('') + this.lastname.toLowerCase();
        }
    };
}
let user = createNewUser();
console.log(user);
alert(user.getLogin());*/
