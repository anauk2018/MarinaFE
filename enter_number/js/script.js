/*Get from the user two numbers: min and max
Output all the even numbers between min and max (note that min and max
themselves might be odd numbers)
For example, if the user enters min = 5 and max = 14, you should print the numbers  6,8,10,12,14*/

function validation(value){
    while (isNaN(value)) {
        value = prompt('Enter number');
    }
    return Number(value);
}

let minNumber = prompt('Enter min number!','');
minNumber = validation(minNumber);

let maxNumber = prompt('Enter max number!','');
maxNumber = validation(maxNumber);

while (maxNumber<=minNumber) {
    maxNumber= prompt('Введите число большее'+ minNumber);
}
for (let i=minNumber; i<=maxNumber; i++) {
    if(i%2 == 0) {
        console.log(i);
    }
}