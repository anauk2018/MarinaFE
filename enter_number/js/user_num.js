/*Get from the user a number
Print to the console a square of stars whose length is the number specified by the user
For example, if the user entered the number 15, your should print:
Hint: Use the character ‘\n’ to start a new line*/

let number = prompt('Enter number');
while(isNaN(number)) {
    number = prompt('Enter number number');
}
number = Number(number);
for (let i=0; i<number; i++){
    let line = '';
    for(let j=0; j<number; j++){
        line +='*';
    }
    console.log(line);
    console.log("\n");
}