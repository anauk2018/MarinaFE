/*Пользователь вводит диапазон нужно вывести на экран все простые числа из этого диапазона. Простыми числами называются числа, которые имеют всего 2 делителя, то есть
03
делятся только на 1 и на самого себя! Пример простых чисел: 3,5,7,11,13,17,...*/

function simpleValueRange(start, end) {
    let sum ='';
    for (let i=start; i<end; i++) {
        let isSimple = true;
        for(let j=2; j<i; j++){            
            if (i%j == 0){
                isSimple = false;                
                break;
            } 
        }
        if(isSimple){
            sum += String(i) + " ";
        }
    }
    console.log(sum);
    return true;
}

let start = prompt('Enter start diapason');
start = Number(start);

let end = prompt('Enter end diapason');
end = Number(end);

simpleValueRange(start, end);