/*Найти факториал числа, у пользователя запрашивается число, и в результате выдаётся факториал числа.
Если ввести к примеру 5 то результат должен быть равен 120, потому что факториал 5 это 5*4*3*2*1.*/

let number = prompt("Введите число");
number = Number(number);
let result = 1;
for (let i=1; i<=number;i++){
    result = result*i;
}
console.log(result);